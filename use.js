// import Queue from './Queue';
const Queue = require('./Queue');

const q = new Queue();

// Enqueuing numbers 1-10
for (let i = 1; i <= 10; i++) {
    q.enqueue(i);
}

// Get the first item (should return 1)
console.log(q.peek());

// Remove the first item
q.dequeue();

// Check the first item once again (should return 2)
console.log(q.peek());

// Remove all items from the Queue
q.dequeue_all();

// Peek again (should return undefined)
console.log(q.peek());

// Enqueue 1, 2, and 3
q.enqueue(1);
q.enqueue(2);
q.enqueue(3);

// Peek one last time (should return 1)
console.log(q.peek());