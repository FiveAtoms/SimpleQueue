class Queue {
    constructor() {
        this._elements = [];
    }

    enqueue(e) {
        this._elements.push(e);
    }

    dequeue() {
        return this._elements.shift();
    }

	dequeue_all() {
		while (!this.isEmpty()) this.dequeue()
	}

    isEmpty() {
        return this._elements.length == 0;
    }

    peek() {
        return !this.isEmpty() ? this._elements[0] : undefined;
    }

    length() {
        return this._elements.length;
    }
}

module.exports = Queue;